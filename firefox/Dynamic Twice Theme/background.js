var currentTheme = "";// To hold the id/name of the currently selected theme

const defaultThemeKey = "TW";// Holds the default value for the theme id/name

// Object to store the different theme profiles
const themes = {
	"dark": {
		colors: {
			"button_background_active": "#333333",
    		"button_background_hover": "#282828",
    		"bookmark_text": "white",
    		"frame": "#000000",
    		"frame_inactive": "#000000",
    		"icons": "#e6e6e6",
    		"ntp_background": "#000000",
    		"ntp_text": "white",
    		"popup": "#181818",
    		"popup_border": "#303030",
    		"popup_highlight": "#303030",
    		"popup_highlight_text": "white",
    		"popup_text": "white",
    		"sidebar": "#181818",
    		"sidebar_border": "#303030",
    		"sidebar_highlight": "#303030",
    		"sidebar_highlight_text": "white",
    		"sidebar_text": "white",
    		"tab_background_separator": "#303030",
    		"tab_loading": "white",
    		"tab_selected": "#181818",
    		"tab_text": "white",
    		"tab_background_text": "white",
    		"toolbar": "#181818",
    		"toolbar_bottom_separator": "#181818",
    		"toolbar_field": "#000000",
    		"toolbar_field_border": "#181818",
    		"toolbar_field_border_focus": "#303030",
    		"toolbar_field_focus": "#111111",
    		"toolbar_field_highlight": "#333333",
    		"toolbar_field_highlight_text": "white",
    		"toolbar_field_separator": "#181818",
    		"toolbar_field_text": "white",
    		"toolbar_field_text_focus": "white",
    		"toolbar_top_separator": "#181818",
    		"toolbar_vertical_separator": "#181818"
		}
	},
	"TW": {
		colors: {
			// #FCC89B, rgb(252,200,155), PANTONE 712C
			"tab_line": "#FCC89B",
			"icons_attention": "#FCC89B"
		}
	},
	"NY": {
		colors: {
			// #80CAF1, rgb(128,202,241)
			"tab_line": "#80CAF1",
			"icons_attention": "#80CAF1"
		}
	},
	"JY": {
		colors: {
			// #BCD776, rgb(188,215,118)
			"tab_line": "#BCD776",
			"icons_attention": "#BCD776"
		}
	},
	"MM": {
		colors: {
			// #F8CFD7, rgb(248,207,215)
			"tab_line": "#F8CFD7",
			"icons_attention": "#F8CFD7"
		}
	},
	"SN": {
		colors: {
			// #9C9ECF, rgb(156,158,207)
			"tab_line": "#9C9ECF",
			"icons_attention": "#9C9ECF"
		}
	},
	"JH": {
		colors: {
			// #FAC857, rgb(250,200,87)
			"tab_line": "#FAC857",
			"icons_attention": "#FAC857"
		}
	},
	"MN": {
		colors: {
			// #6FC5C2, rgb(111,197,194)
			"tab_line": "#6FC5C2",
			"icons_attention": "#6FC5C2"
		}
	},
	"DH": {
		colors: {
			// #FFFFFF, rgb(255,255,255)
			"tab_line": "#FFFFFF",
			"icons_attention": "#FFFFFF"
		}
	},
	"CH": {
		colors: {
			// #E81933, rgb(232,25,51)
			"tab_line": "#E81933",
			"icons_attention": "#E81933"
		}
	},
	"TY": {
		colors: {
			// #016CBA, rgb(1,108,186)
			"tab_line": "#016CBA",
			"icons_attention": "#016CBA"
		}
	},
	"TW-ALT": {
		colors: {
			// #FF5FA2, rgb(255,95,162), PANTONE 812C
			"tab_line": "#FF5FA2",
			"icons_attention": "#FF5FA2"
		}
	}
};

// Array of events with their start and end dates in MMDD
const events = [
	{id:"NY", startDate:"0922", endDate:"0923"},
	{id:"JY", startDate:"1101", endDate:"1102"},
	{id:"MM", startDate:"1109", endDate:"1110"},
	{id:"SN", startDate:"1229", endDate:"1230"},
	{id:"JH", startDate:"0201", endDate:"0202"},
	{id:"MN", startDate:"0324", endDate:"0325"},
	{id:"DH", startDate:"0528", endDate:"0529"},
	{id:"CY", startDate:"0423", endDate:"0424"},
	{id:"TY", startDate:"0614", endDate:"0615"},
	{id:"TW-ALT", startDate:"1020", endDate:"1021"}
];

// To convert dates stored in events into dates that can be compared to output from Date obj
const eventToDate = (date) => (10000 * (parseInt(date) - 100) + 900);

// To set the theme from the id/name parameter
function setTheme(themeId) {
	if (currentTheme === themeId) {
		// No point in changing the theme if it has already been set.
		return;
	}
	currentTheme = themeId;
	
	// merge event theme with main 
	let newTheme = themes.dark;
	newTheme.colors.tab_line = themes[themeId].colors.tab_line;
	newTheme.colors.icons_attention = themes[themeId].colors.icons_attention;
	browser.theme.update(newTheme);
}

function checkDateTime() {
	// Get current date and time with padding to 2 digits
	let fullDate = new Date();
	let month = (fullDate.getUTCMonth()+"").padStart(2,0);
	let date = (fullDate.getUTCDate()+"").padStart(2,0);
	let hour = (fullDate.getUTCHours()+"").padStart(2,0);
	let minute = (fullDate.getUTCMinutes()+"").padStart(2,0);
	let dateTime = parseInt(month+date+hour+minute);

	// Clycle through events to see if any match current date
    for (let i = 0; i < events.length; i++) {
	    if (dateTime >= eventToDate(events[i].startDate) && dateTime < eventToDate(events[i].endDate)) {
			setTheme(events[i].id);
        	return;
    	}
	}
	
	setTheme(defaultThemeKey);
}

// On start up, check the time to see what theme to show.
checkDateTime();

// Set up an alarm to check this regularly
browser.alarms.onAlarm.addListener(checkDateTime);
browser.alarms.create("checkDateTime", {periodInMinutes: 10});